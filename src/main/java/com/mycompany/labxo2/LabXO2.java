/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.labxo2;
import java.util.Scanner;
/**
 *
 * @author armme
 */
public class LabXO2 {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static int row,col;
    static char player = 'o';
    public static void showWelcometoXO(){
        System.out.println("Welcome to XO Game");
    }
    public static void showTable(){
        for(int i = 0 ; i < table.length; i++ ){
            for(int j = 0; j < table.length; j ++ ){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    public static void inputPosition(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Player " +  player + " input position: ");
        row = kb.nextInt();
        col = kb.nextInt();
        table[row][col] = player;
    }
    public static void switchPlayer(){
        if (player == 'o')
            player = 'x';
        else
            player = 'o';
    }
    static boolean checkRow(){
        for (int c = 0; c < 3; c++){
            if (table[row][c] != player)
                return false;
        }
        return true;
    }
    static boolean checkCol(){
        for (int r = 0; r < 3; r++){
            if (table[r][col] != player)
                return false;
        }
        return true;
    }
    static boolean checkX1(){
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player){
            return true;
        }
        return false;
    }
    static boolean checkX2(){
        if (table[0][2] == player && table[1][1] == player && table[2][2] == player){
            return true;
        }
        return false;
    }
    static boolean checkWin(){
        if (checkRow()||checkCol())
            return true;
        if (checkX1() || checkX2())
            return true;
        return false;
    }
    public static void showWin() {
        System.out.println(player+" Winnnnn!!!!!");
    }
}
